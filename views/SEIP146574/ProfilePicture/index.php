<head>
  <link rel="stylesheet" href="../../../resource/assets/bootstrap/css/bootstrap.min.css">
  <link rel="stylesheet" href="../../../resource/assets/font-awesome/css/font-awesome.min.css">
  <script src="../../../resource/assets/bootstrap/js/bootstrap.min.js"></script>
  <script src="../../../resource/assets/js/jquery.backstretch.min.js"></script>
  <img src="../../../resource/assets/img/backgrounds/table.jpg" style="position: absolute; margin: 0px; padding: 0px; border: none; width: 100%; height: 100%;  z-index: -999999; top: 0px;">

  <style>
        table{
          text-align: center;
          margin :50px auto;
          background-color:rgba(183, 179, 179, 0.15);
        }
    th{
      text-align: center;
      height: 32px;
      color: #5e90c3;
      font-family: serif;
      font-size: 19px;
    }
    td,tr{
       padding: 20px;
      text-align: center;
     }
  </style>

  <script language="JavaScript" type="text/javascript">
    function checkDelete()
    {
      return confirm("Are you sure you want to delete");
    }
  </script>
</head><?php
require_once("../../../vendor/autoload.php");

use App\ProfilePicture\ProfilePicture;
use App\Message\Message;
if(!isset($_SESSION))session_start();
echo "<div id =\"message\">". Message::message()."</div>";

$objProfilePicture=new ProfilePicture;

$allData=$objProfilePicture->index("obj");
?>
<a href='create.php' style="margin-left: 1100px;"><button class='btn btn-success'>Add New</button></a>
<a href='trashList.php'><button class='btn btn-primary'>Trashed List</button></a>

<?php
$serial=1;
echo "<table>";
echo "<th> Serial No</th><th > ID</th><th>Name</th><th>Profile Picture</th><th>Action </th>";
foreach($allData as $oneData)

{
  echo "<tr style='height: 40px'>";
  echo "<td> $serial</td>";
  echo "<td> $oneData->id</td>";
  echo "<td> $oneData->name</td>";
  echo "<td> $oneData->image</td>";
  echo"
  <td>
        <a href='view.php?id=$oneData->id'><button class='btn btn-info'>View</button></a>
        <a href='edit.php?id=$oneData->id'><button class='btn btn-success'>Edit</button></a>
        <a href='trash.php?id=$oneData->id'><button class='btn btn-primary'>Trash</button></a>
        <a href='delete.php?id=$oneData->id'onclick='return checkDelete()'><button class='btn btn-danger'>Delete</button></a>

   </td>
  ";

  echo "</tr>";
  $serial++;
}
echo "</table>";
?>
<script>
  $('#message').show().delay(10).fadeOut();
  $('#message').show().delay(10).fadeIn();
  $('#message').show().delay(10).fadeOut();
  $('#message').show().delay(10).fadeIn();
  $('#message').show().delay(1200).fadeOut();
</script>